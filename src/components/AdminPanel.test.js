import React from 'react';
import ReactDOM from 'react-dom';
import AdminPanel from './AdminPanel';

import {configure} from 'enzyme';
import { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16'; 

configure({adapter: new Adapter()});

describe('Admin Panel tests', () => {

  it('Admin Panel renders without a problem', () => {
    const div = document.createElement('div');
    ReactDOM.render(<AdminPanel />, div);
    ReactDOM.unmountComponentAtNode(div);
  })
  
  it('Zamowienie renders', () => {
    const wrapper = shallow(<AdminPanel/>); 
    //console.log(wrapper.debug());
    expect(wrapper.find('div').text()).toBe('Panel Administratora');
  })
})
