import React from "react";
import AdminPanel from "./AdminPanel";
import Order from "./Order";
import Header from "./Header";
import Inventory from "./Inventory";

import "../index.css";

class App extends React.Component {
  render() {
    return (
      <div className="app container">
        <Header />
        <div className="navbar">
          <Order />
          <Inventory />
          <AdminPanel />
        </div>
      </div>
    );
  }
}
// export default Inventory;
// export default Headewr;
// export default Order;
// export default AdminPanel;
export default App;
